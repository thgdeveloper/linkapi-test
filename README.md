# LinkApi Test



### Setup

```bash
npm install
```

### Starting the project

```bash
npm start
```
### Rota de integração pedidos
#### Retorno : 204 - No content

```
@post 
http://localhost:8000/api/check/deals/orders
```

### Rota para exibir os pedidos cadastrados
#### Retorno : 200 - Array de pedidos e metadados
```
@get 
http://localhost:8000/api/dealsorder

@params 
limit (int) default 10
page (int)
```


### Atenção
Necessário pré cadastrar novos pedidos no Pipedrive e alterá-los para ganho , o sistema irá ignorar o cadastro de pedidos já cadastrados , porém exibirá apenas no  `console.log`
