const express = require('express');
const bodyParser = require('body-parser');
const routes = require('./routes');

const app = express();
const PORT =  8000;


var jsonParser = bodyParser.json();
var parseUrlencoded = bodyParser.urlencoded({ extended: false });


app.use(jsonParser);
app.use(parseUrlencoded);
app.use('/api',routes)


app.listen(PORT, function() {
    console.log(`Server listen at port ${PORT}`)
});