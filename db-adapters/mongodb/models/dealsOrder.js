'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var DealsOrderSchema = new Schema({
    cliente:{
        nome:String
    },
    data : String,
    data_prevista : String,
    data_saida : String,
    itens :{
        item :{
            codigo : String,
            descricao : String,
            qtde : Number,
            un : Number,
            vlr_unit : Number
        }
    },
    loja: Number,
    numero : Number,
    numero_loja : Number
});

module.exports = mongoose.model('DealsOrder',DealsOrderSchema,'dealsOrder');