const config = require('./../../config/').mongo;
var mongoose = require('mongoose');
var fs = require('fs');

mongoose.connect(config.MONGODB_RESOURCE ,{ useNewUrlParser : true });

var db = mongoose.connection;

fs.readdirSync(__dirname + '/models').forEach((filename) => {
    console.log(filename);
    if(~filename.indexOf('.js')) require(__dirname + '/models/' + filename );
})

db.on('error',function(){
    console.error.bind(console,'connection error:');
    throw new Error('Failed to connect mongodb');
});

db.once('open',function(){
    console.log('Connection successfully with mongodb');
});
exports.db = mongoose.connection;
