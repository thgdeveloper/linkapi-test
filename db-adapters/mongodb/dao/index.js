const clientMongoDb = require('./../../../db-adapters/mongodb').db;

exports.saveOrderMongoDb = (order) => {
    return clientMongoDb.models.DealsOrder.create(order);
}

exports.getDealsOrders = (options) => {
    return new Promise((resolve, reject) => {
        clientMongoDb.models.DealsOrder.find()
        .limit(options.limit)
        .skip(options.skip)
        .sort( {'_id':-1} )
        .exec((err,dealsOrder) => {
            if(err){
                return reject(err);
            }
            return resolve(dealsOrder);
        });
    });
}

