const axios = require('axios');
class Pipedrive {
    constructor(){
        this.apiUrl = 'https://api.pipedrive.com/v1';
        this.apiToken = '53de45b223f540bf3773946ba698805e3d389270';
    }

    getPipelines(){
        return new Promise((resolve, reject) => {
            const resource = `${this.apiUrl}/pipelines`
            axios.get(resource, {
                params: {
                  api_token: this.apiToken
                }
              })
              .then(function (response) {
                  const objReturn = response.data || {};
                return resolve(objReturn);
              })
              .catch(function (error) {
                 return reject(error);
              })
        })
    }

    getDeals(status){
        return new Promise((resolve, reject) => {
            const resource = `${this.apiUrl}/deals`
            axios.get(resource, {
                params: {
                    status,
                    api_token: this.apiToken
                }
              })
              .then(function (response) {
                  const objReturn = response.data || false;
                  if(objReturn && objReturn.success){
                    return resolve(objReturn.data);
                  }else{
                    return reject('Erro ao buscar registros');
                  }
                
              })
              .catch(function (error) {
                 return reject(error);
              })
        })
    }

}

module.exports = Pipedrive;