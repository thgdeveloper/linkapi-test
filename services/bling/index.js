const axios = require('axios');
var jsonxml = require('jsontoxml');
const request = require("request");
const mongodb = require('./../../db-adapters/mongodb/dao');

class Bling {
    constructor(){
        this.apiUrl = 'https://bling.com.br/Api/v2';
        this.apiToken = '38ee37465ea5a806a056b1b9224d19b828a4dc5512391c53f94c8c10bfa23e310a92c072';
    }

    getProducts(){
        return new Promise((resolve, reject) => {
            const resource = `${this.apiUrl}/produtos/json`
            axios.get(resource, {
                params: {
                  apikey: this.apiToken
                }
              })
              .then(function (response) {
                  const objReturn = response.data || {};
                return resolve(objReturn);
              })
              .catch(function (error) {
                 return reject(error);
              })
        })
    }

    _generateOrders(deals){
        const orders = deals.map((deal) => {
          const order = {
            pedido : {
              data : deal.add_time,
              data_saida: deal.add_time,
              data_prevista: deal.add_time,
              numero : deal.stage_order_nr,
              numero_loja :deal.stage_order_nr,
              loja :2,
              cliente :{
                nome: deal.creator_user_id.name,
              },
              itens:{
                item :{
                  codigo : '2',
                  descricao : deal.title,
                  un : deal.products_count,
                  qtde : deal.products_count,
                  vlr_unit : parseFloat(deal.value || 0)
                }
               
              },
            }
          }

          const orderStructure = {
            order,
            xml : this._createXMLStructure(order)
          }

            return orderStructure;
        });

        return orders;
    }

    async insertOrders(deals){
      try {
        const orders = this._generateOrders(deals);


        const promises = orders.map((order) => {
          return this.insertOrder(order);
        });

        const resolvedPromisses = await Promise.all(promises);
        return resolvedPromisses;

      } catch (error) {
        console.log('trycatch error');
        console.log(error);
      }
    
     
    }

    insertOrder(param){
      return new Promise((resolve, reject) => {
        
        const resource = `${this.apiUrl}/pedido/json`;
        var options = { 
              method: 'POST',
              gzip: true,
              url: resource,
              headers: {
                'cache-control': 'no-cache',
                Connection: 'keep-alive',
                'Accept-Encoding': 'gzip',
                Host: 'bling.com.br',
                'Cache-Control': 'no-cache',
                'Accept-Encoding': 'gzip',
                'Content-Type': 'application/x-www-form-urlencoded' 
              },
              form: {
                  xml: param.xml,
                  apikey: this.apiToken
              } 
          };

          request(options, (error, response, body) => {
            if (error) return reject(error);

            const objResponse = JSON.parse(body);

            if(objResponse.retorno && objResponse.retorno.erros){
              return reject(objResponse.retorno.erros[0].erro.msg);
            }
            
            if(objResponse.retorno && objResponse.retorno.pedidos){
              this.saveOrderMongoDb(param.order.pedido)
              .then((response) => {
                return resolve(objResponse);
              })
              .catch((error) => {
                return reject(error);
              })
            }
          });
      });
    }
    
    saveOrderMongoDb(order){
      return mongodb.saveOrderMongoDb(order);
    }

    _createXMLStructure(deal){
     const xml = '<?xml version="1.0" encoding="UTF-8"?>' + jsonxml(deal);
      return xml;
    }
}

module.exports = Bling;






/*pedido : {
  data :1,
  data_saida:1,
  data_prevista:1,
  numero : 1,
  numero_loja :1,
  loja :1,
  nat_operacao : 1,
  vendedor : 1,
  cliente :{
    id : 1,
    nome:'',
    tipoPessoa :'',
    cpf_cnpj:1,
    ie:1,
    rg:1,
    contribuinte:1,
    endereco:1,
    numero: 1,
    complemento:1,
    bairro:1,
    cep:1,
    cidade:1,
    uf:1,
    fone:1,
    calular:1,
    email:''
  },
  transporte : {
    transportadora:'',
    tipo_frete:'',
    servico_correios:1,
    peso_bruto:1,
    dados_etiqueta:{
      nome : 1,
      endereco : 1,
      numero : 1,
      complemento: '',
      municipio: '',
      uf: '',
      cep: '',
      bairro: ''
    },
    volumes :{
      volume : 1,
      servico : 1,
      codigoRastreamento : 1
    }
  },
  itens:{
    item :{
      codigo : 1,
      descricao : 1,
      un : 1,
      qtde : 1,
      vlr_unit : 1
    }
   

  },
  idFormaPagamento: 1,
  parcelas :{
    parcela : {
      dias : 1,
      data : 1,
      vlr : 1,
      obs : 1,
      forma_pagamento :{
        id:1
      }
    }
   

  },
  vlr_frete :1,
  vlr_desconto :1,
  obs :'',
  obs_internas:'',
  numeroOrdemCompra:''
}*/