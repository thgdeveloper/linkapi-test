let express = require('express');
let router = express.Router();
const apiController = require('./../controllers/apiController');

router.post('/check/deals/orders', apiController.checkDealOrders);
router.get('/dealsorder', apiController.getDealsOrders);

module.exports = router;
