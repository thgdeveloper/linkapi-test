const PipedriveServices = require('./../services/pipedrive');
const BlingServices = require('./../services/bling');
const mongodb = require('./../db-adapters/mongodb/dao');

exports.checkDealOrders = (req, res, next) => { 

    const status = req.query.status || "won";

    const PipedriveService = new PipedriveServices();
    const BlingService = new BlingServices();
    PipedriveService.getDeals(status)
    .then((deals) => {
       if(deals){
           return BlingService.insertOrders(deals)
       }else{
           res.end('Deals not found');
       }
    })
    .then(() => {
        res.status(204).send();
    })
    .catch((error) => {
        console.log(error);
        return res.end(error.message);
    }) 
}

exports.getDealsOrders = (req, res, next) => { 
    const page = parseInt(req.query.page || 0);
    const limit = parseInt(req.query.limit || 10);
    const skip = parseInt((page > 0 ? (page * limit) : 0));

    const options = {
        skip,
        limit
    }

    mongodb.getDealsOrders(options)
    .then((dealsOrder) => {
        const defaultReturn = {
            data :[],
            metadata : {
                page,
                limit,
                skip
            }
        };

        if(dealsOrder){
            defaultReturn.data = dealsOrder;
        }

        res.send(defaultReturn);
    })
    .catch((error) => {
        console.log(error);
        return res.end(error.message);
    }) 
}